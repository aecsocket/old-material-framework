package me.aecsocket.materialframework.util;

import me.aecsocket.materialframework.MaterialFramework;
import me.aecsocket.materialframework.exception.ItemAdaptException;
import org.bukkit.NamespacedKey;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

/** Generic utils for custom items. */
public final class ItemUtils {
    /** The name of the type key. */
    public static final String TYPE_KEY_NAME = "type";
    /** The type key. */
    private static NamespacedKey TYPE_KEY;

    static {
        TYPE_KEY = new NamespacedKey(MaterialFramework.getInstance(), TYPE_KEY_NAME);
    }

    /** Gets the type key.
     @return The type key.
     */
    public static NamespacedKey getTypeKey() { return TYPE_KEY; }

    /** Saves the string type to a {@link PersistentDataContainer}.
     @param container The {@link PersistentDataContainer}.
     @param type The string type.
     */
    public static void saveType(PersistentDataContainer container, String type) {
        container.set(TYPE_KEY, PersistentDataType.STRING, type);
    }

    /** Gets the type from a {@link PersistentDataContainer}.
     @param container The {@link PersistentDataContainer}.
     @return The string type.
     */
    public static String getType(PersistentDataContainer container) {
        if (container.has(TYPE_KEY, PersistentDataType.STRING))
            return container.get(TYPE_KEY, PersistentDataType.STRING);
        return null;
    }

    /** Gets a key from a {@link PersistentDataContainer} with error handling.
     @param container The {@link PersistentDataContainer}.
     @param key The {@link NamespacedKey}.
     @param type The {@link PersistentDataType}.
     @param <T> The primitive type.
     @param <Z> The complex type.
     @return The value in the key.
     @throws ItemAdaptException If the key does not exist.
     */
    public static <T, Z> Z get(PersistentDataContainer container, NamespacedKey key, PersistentDataType<T, Z> type) throws ItemAdaptException {
        if (container.has(key, type))
            return container.get(key, type);
        throw new ItemAdaptException(String.format("Key '%s' of type '%s' does not exist", key, type));
    }

    /** Gets an {@link Enum} from a {@link PersistentDataContainer} with error handling.
     @param container The {@link PersistentDataContainer}.
     @param key The {@link NamespacedKey}.
     @param enumType The {@link Enum}'s type.
     @param <T> The {@link Enum}'s type.
     @return The {@link Enum}.
     @throws ItemAdaptException If the key does not exist, or the value is invalid.g
     */
    @SuppressWarnings("unchecked")
    public static <T extends Enum> T getEnum(PersistentDataContainer container, NamespacedKey key, Class<T> enumType) throws ItemAdaptException {
        String enumName = get(container, key, PersistentDataType.STRING);
        if (enumName != null) {
            try {
                return (T)Enum.valueOf(enumType, enumName);
            } catch (IllegalArgumentException e) {
                throw new ItemAdaptException(String.format("Invalid enum value '%s'", enumName));
            }
        }
        throw new ItemAdaptException(String.format("Key '%s' is null", key));
    }
}
