package me.aecsocket.materialframework.block;

import org.bukkit.Location;
import org.bukkit.block.data.BlockData;

/** The custom block. */
public interface Block {
    /** Gets the type of the block.
     @return The type of the block.
     */
    String getType();

    /** Gets the {@link Location} of the block.
     @return The {@link Location} of the block.
     */
    Location getLocation();

    /** Gets the {@link BlockData} of the block.
     @return The {@link BlockData} of the block.
     */
    BlockData getData();
}
