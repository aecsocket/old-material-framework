package me.aecsocket.materialframework.block;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import me.aecsocket.materialframework.MaterialFramework;
import me.aecsocket.pluginutils.json.JsonUtils;
import me.aecsocket.pluginutils.json.spigot.SpigotAdapters;
import org.bukkit.Chunk;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static me.aecsocket.resourceframework.ResourceFramework.*;

/** The class that loads and unloads custom blocks. */
public class BlockManager {
    public static final String TYPE_KEY = "type";
    public static final String LOCATION_KEY = "location";
    public static final String BLOCK_KEY = "location";

    /** The file name format for chunk data. */
    public static final String FILE_NAME = "%s.%s.%s.json";

    private GsonBuilder gsonBuilder;
    /** A {@link Map} of block type names linked to their {@link Block} classes. */
    private Map<String, Class<? extends Block>> registeredClasses;
    /** A {@link Map} of all loaded {@link Chunk}s on the server, mapped to a {@link Map} of Bukkit
     {@link org.bukkit.block.Block}s linked to custom {@link Block}s. */
    private Map<Chunk, Map<org.bukkit.block.Block, Block>> loadedChunks;

    public BlockManager() {
        this.gsonBuilder = SpigotAdapters.GSON_BUILDER.setPrettyPrinting();
        this.registeredClasses = new HashMap<>();
        this.loadedChunks = new HashMap<>();
    }

    public GsonBuilder getGsonBuilder() { return gsonBuilder; }

    /** Gets a {@link Map} of block type names linked to their {@link Block} classes.
     @return A {@link Map} of block type names linked to their {@link Block} classes.
     */
    public Map<String, Class<? extends Block>> getRegisteredClasses() { return registeredClasses; }
    /** Gets a {@link Map} of all loaded {@link Chunk}s on the server, mapped to a {@link Map} of Bukkit
     {@link org.bukkit.block.Block}s linked to custom {@link Block}s.
     @return A {@link Map} of all loaded {@link Chunk}s on the server, mapped to a {@link Map} of Bukkit
     {@link org.bukkit.block.Block}s linked to custom {@link Block}s. */
    public Map<Chunk, Map<org.bukkit.block.Block, Block>> getLoadedChunks() { return loadedChunks; }

    /** Registers a block type name linked to its {@link Block} class.
     @param type The block type name.
     @param clazz The {@link Block} class.
     */
    public void registerType(String type, Class<? extends Block> clazz) { registeredClasses.put(type, clazz); }

    /** Gets all loaded custom blocks, regardless of chunks.
     @return All loaded custom blocks, regardless of chunks.
     */
    public Map<org.bukkit.block.Block, Block> getLoadedBlocks() {
        Map<org.bukkit.block.Block, Block> map = new HashMap<>();
        for (Chunk chunk : loadedChunks.keySet())
            map.putAll(loadedChunks.get(chunk));

        return map;
    }

    /** Loads the disk data of a {@link Chunk} into memory (into {@link BlockManager#loadedChunks}).
     @param chunk The {@link Chunk} to load disk data from.
     */
    public void load(Chunk chunk) {
        String worldUID = chunk.getWorld().getUID().toString();
        String fileName = String.format(FILE_NAME, worldUID, chunk.getX(), chunk.getZ());
        String json = getStringData(MaterialFramework.getInstance(), worldUID, fileName);

        if (json != null) {
            Gson gson = gsonBuilder.create();

            List<JsonObject> jsonArray = gson.fromJson(json, new TypeToken<List<JsonObject>>() {}.getType());
            if (jsonArray.size() > 0) {
                Map<org.bukkit.block.Block, Block> thisChunk = loadedChunks.getOrDefault(chunk, new HashMap<>());

                for (JsonObject jsonObject : jsonArray) {
                    String type = JsonUtils.get(jsonObject, TYPE_KEY).getAsString();
                    org.bukkit.block.Block bukkitBlock = gson.fromJson(JsonUtils.get(jsonObject, LOCATION_KEY), Location.class).getBlock();
                    if (registeredClasses.containsKey(type)) {
                        Block newBlock = gson.fromJson(jsonObject, registeredClasses.get(type));
                        thisChunk.put(bukkitBlock, newBlock);
                    }
                }

                loadedChunks.put(chunk, thisChunk);
            }
        }
    }

    /** Unloads all data from a {@link Chunk} from memory into disk.
     @param chunk The {@link Chunk} to unload data from.
     */
    public void unload(Chunk chunk) {
        String worldUID = chunk.getWorld().getUID().toString();
        String fileName = String.format(FILE_NAME, worldUID, chunk.getX(), chunk.getZ());

        if (loadedChunks.containsKey(chunk)) {
            Map<org.bukkit.block.Block, Block> thisChunk = loadedChunks.get(chunk);
            Gson gson = gsonBuilder.create();
            JsonArray array = new JsonArray();

            for (org.bukkit.block.Block bukkitBlock : thisChunk.keySet()) {
                Block block = thisChunk.get(bukkitBlock);
                String type = block.getType();
                if (registeredClasses.containsKey(type)) {
                    JsonElement element = gson.toJsonTree(block, registeredClasses.get(type));

                    JsonObject jsonObject;

                    if (element.isJsonObject())
                        jsonObject = element.getAsJsonObject();
                    else {
                        jsonObject = new JsonObject();
                        jsonObject.add(BLOCK_KEY, element);
                    }

                    jsonObject.addProperty(TYPE_KEY, block.getType());
                    jsonObject.add(LOCATION_KEY, gson.toJsonTree(block.getLocation()));
                    array.add(jsonObject);
                }
            }

            saveStringData(MaterialFramework.getInstance(), gson.toJson(array), false, worldUID, fileName);
        }
    }
}
