package me.aecsocket.materialframework;

import me.aecsocket.materialframework.block.Block;
import me.aecsocket.materialframework.block.BlockManager;
import me.aecsocket.materialframework.item.Item;
import me.aecsocket.materialframework.item.ItemAdapter;
import me.aecsocket.materialframework.util.ItemUtils;
import org.bukkit.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

/** The main plugin class. */
public class MaterialFramework extends JavaPlugin {
    /** The instance of this plugin. */
    private static MaterialFramework instance;

    /** A {@link Map} of registered {@link ItemAdapter}s linked to their string types. */
    private static Map<String, ItemAdapter> adapters;
    private static BlockManager blockManager;

    /** Gets the instance of this plugin.
     @return The instance of this plugin.
     */
    public static MaterialFramework getInstance() { return instance; }

    /** Gets a {@link Map} of registered {@link ItemAdapter}s linked to their string types.
     @return A {@link Map} of registered {@link ItemAdapter}s linked to their string types.
     */
    public static Map<String, ItemAdapter> getAdapters() { return adapters; }

    /** Gets a {@link Map} of loaded {@link Block}s linked to their real {@link org.bukkit.block.Block}s.
     @return A {@link Map} of loaded {@link Block}s linked to their real {@link org.bukkit.block.Block}s.
     */
    public static BlockManager getBlockManager() { return blockManager; }

    @Override
    public void onEnable() {
        instance = this;
        adapters = new HashMap<>();
        blockManager = new BlockManager();

        getServer().getPluginManager().registerEvents(new EventHandle(), this);

        for (World world : Bukkit.getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                blockManager.load(chunk);
            }
        }
    }

    @Override
    public void onDisable() {
        for (World world : Bukkit.getWorlds()) {
            for (Chunk chunk : world.getLoadedChunks()) {
                blockManager.unload(chunk);
            }
        }
    }

    /** Gets an {@link Item} from an {@link ItemStack}.
     @param item The {@link ItemStack} to convert.
     @param type The type of the {@link Item}.
     @param <T> The type of the {@link Item}.
     @return The converted {@link Item}.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Item> T getItem(ItemStack item, Class<T> type) {
        ItemMeta meta = item.getItemMeta();
        if (meta != null) {
            String sType = ItemUtils.getType(meta.getPersistentDataContainer());
            if (type != null) {
                if (adapters.containsKey(sType))
                    return (T)adapters.get(sType).deserialize(item);
            }
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T extends Block> T getBlock(org.bukkit.block.Block block, Class<T> type) {
        if (block != null) {
            Chunk chunk = block.getChunk();
            if (blockManager.getLoadedChunks().containsKey(chunk) && blockManager.getLoadedChunks().get(chunk).containsKey(block))
                return (T)blockManager.getLoadedChunks().get(chunk).get(block);
        }

        return null;
    }

    /** Registers an {@link ItemAdapter} linked to a string type.
     @param type The string type.
     @param adapter The {@link ItemAdapter}.
     */
    public static void registerItemAdapter(String type, ItemAdapter adapter) {
        adapters.put(type, adapter);
    }

    /** Registers a {@link Block} {@link Class} linked to a string type.
     @param type The string type.
     @param clazz The {@link Block} {@link Class}.
     */
    public static void registerBlockType(String type, Class<? extends Block> clazz) {
        blockManager.registerType(type, clazz);
    }
}
