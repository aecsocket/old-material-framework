package me.aecsocket.materialframework.item;

import me.aecsocket.materialframework.exception.ItemAdaptException;
import org.bukkit.inventory.ItemStack;

/** Converts an {@link ItemStack} to a custom {@link Item}.
 @param <T> The type of {@link Item} that it converts to.
 */
public interface ItemAdapter<T extends Item> {
    /** Convert an {@link ItemStack} to {@link T}.
     @param item The {@link ItemStack}.
     @return The converted {@link T}.
     @throws ItemAdaptException If it cannot adapt the {@link ItemStack} to {@link T}.
     */
    T deserialize(ItemStack item) throws ItemAdaptException;
}
