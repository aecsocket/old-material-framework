package me.aecsocket.materialframework.item;

import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/** The custom item. */
public interface Item {
    /** Gets the type of the item.
     @return The type of the item.
     */
    String getType();

    /** Gets the {@link ItemStack} this represents as to a player. This should have enough data to convert back to this class using an
     {@link ItemAdapter}. Consider using the {@link org.bukkit.inventory.meta.ItemMeta}'s
     {@link org.bukkit.persistence.PersistentDataContainer}.
     @param player The {@link Player} to generate this to. This can be null, in which case it should use default
     options.
     @return The {@link ItemStack}.
     */
    ItemStack getHandle(@Nullable Player player);
}
