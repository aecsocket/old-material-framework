package me.aecsocket.materialframework;

import me.aecsocket.materialframework.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

public class EventHandle implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onChunkLoad(ChunkLoadEvent event) {
        MaterialFramework.getBlockManager().load(event.getChunk());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onChunkUnload(ChunkUnloadEvent event) {
        MaterialFramework.getBlockManager().unload(event.getChunk());
    }

    private void destroyBlock(org.bukkit.block.Block bukkitBlock) {
        Block block = MaterialFramework.getBlock(bukkitBlock, Block.class);
        if (block != null)
            MaterialFramework.getBlockManager().getLoadedChunks().get(block.getLocation().getChunk()).remove(bukkitBlock);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBreak(BlockBreakEvent event) { destroyBlock(event.getBlock()); }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBurn(BlockBurnEvent event) { destroyBlock(event.getBlock()); }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onExplode(BlockExplodeEvent event) {
        destroyBlock(event.getBlock());
        for (org.bukkit.block.Block block : event.blockList())
            destroyBlock(block);
    }
}
