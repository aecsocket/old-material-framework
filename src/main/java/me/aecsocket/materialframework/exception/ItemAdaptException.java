package me.aecsocket.materialframework.exception;

public class ItemAdaptException extends RuntimeException {
    public ItemAdaptException(String message) {
        super(message);
    }
}
