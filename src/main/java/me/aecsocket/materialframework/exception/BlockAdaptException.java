package me.aecsocket.materialframework.exception;

public class BlockAdaptException extends RuntimeException {
    public BlockAdaptException(String message) {
        super(message);
    }
}
